﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewComponent", menuName = "Component")]
public class ComponentSO : ScriptableObject {
    
    public string componentName;
    public Sprite icon = null;
    public bool isAutoPickup = true;

    public int power;
    public bool isEquipped = false;

    public Effect[] effects;
    public bool effectIsActive = false;

    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }

}
