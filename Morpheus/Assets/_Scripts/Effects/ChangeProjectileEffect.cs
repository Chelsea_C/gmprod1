﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Change Projectile Effect", menuName = "Effect/ChangeProjectile")]
public abstract class ChangeProjectileEffect : Effect {

    public override void Activate(EffectsHandler handler)
    {
        ChangeProjectile(handler);
    }

    void ChangeProjectile(EffectsHandler handler)
    {

    }
}
