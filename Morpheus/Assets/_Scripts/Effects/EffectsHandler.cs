﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsHandler : MonoBehaviour {

    public List<Effect> effects;

    [HideInInspector] public PlayerController controller;
    [HideInInspector] public PlayerControllerData controllerData;
    [HideInInspector] public PlayerData playerData;
    [HideInInspector] public GameObject projectile;

    void Awake()
    {
        controller = GetComponent<PlayerController>();
        controllerData = controller.data;
        playerData = GetComponent<Player>().data;
    }

    void Start()
    {
        effects = new List<Effect>();
    }

    void Update()
    {
        // If inventory is empty
        if (Inventory.instance.equipped == null)
            return;
    }   

    public void UpdateEffects()
    {
        Debug.Log("Updating effects");
        var inventory = Inventory.instance;
        List<Effect> temp = new List<Effect>();
        foreach(ComponentSO component in inventory.equipped)
        {
            if (inventory.GetCount(component) > 1)
                return;

            foreach (Effect effect in component.effects)
                temp.Add(effect);
        }
    }

    public void SetProjectileColor()
    {

    }

    public void SetProjectileRange()
    {

    }

    public void SetProjectileDamage()
    {

    }


}
