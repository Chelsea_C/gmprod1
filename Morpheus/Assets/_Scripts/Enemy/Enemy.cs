﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(EnemyController), typeof(LootDropper))]
public class Enemy : MonoBehaviour
{
    public EnemyData data;
    public Image healthBar;

    int maxHealth;
    int currentHealth;
    int attack;
    Animator anim;
    EnemyController enemy;
    LootDropper lootDropper;

	void Start ()
    {
        lootDropper = gameObject.GetComponent<LootDropper>();
        anim = gameObject.GetComponentInChildren<Animator>();
        enemy = gameObject.GetComponent<EnemyController>();

        // Initialize stats
        maxHealth = data.init_maxHealth;
        currentHealth = data.init_maxHealth;


        healthBar.fillAmount = 1;
	}
	
	void Update ()
    {
        // Update health bar
        healthBar.fillAmount = (float) currentHealth / maxHealth;

        //TESTING
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(100);
        }

        if (currentHealth <= 0)
            Die();
	}

    public void TakeDamage(int damage)
    {
        Debug.Log(name + " takes " + damage + "damage");
        if (currentHealth > 0)
        {
            currentHealth -= damage;
        }
        else
        {
            Die();
        }
    }

    void Die()
    {

        lootDropper.DropItem();
        Destroy(gameObject);
    }
}
