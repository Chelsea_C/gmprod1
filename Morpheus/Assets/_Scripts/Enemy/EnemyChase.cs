﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : FSMState {

    public EnemyChase()
    {
        stateID = StateID.Chase;
    }

    public override void Reason(GameObject player, GameObject npc)
    {

    }

    public override void Act(GameObject player, GameObject npc)
    {

    }
}
