﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewEnemyData", menuName = "Enemy Data")]
public class EnemyData : ScriptableObject
{
    public int init_maxHealth = 1000;
    public int init_attack = 100;
    public float detectionRadius = 1.0f;
}
