﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFSM : MonoBehaviour {

    public GameObject player;

    FSMSystem fsm;

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

	void Start ()
    {
        if (player == null)
            Debug.Log("Player not set");
        MakeFSM();
	}

    void MakeFSM()
    {
        EnemyIdle idle = new EnemyIdle();
        idle.AddTransition(Transition.DetectPlayer, StateID.Chase);

        EnemyPatrol patrol = new EnemyPatrol();
        patrol.AddTransition(Transition.DetectPlayer, StateID.Chase);

        EnemyChase chase = new EnemyChase();
        chase.AddTransition(Transition.LostPlayer, StateID.Idle);
        chase.AddTransition(Transition.InAttackRange, StateID.Attack);

        EnemyAttack attack = new EnemyAttack();
        attack.AddTransition(Transition.OutOfAttackRange, StateID.Chase);
        attack.AddTransition(Transition.LostPlayer, StateID.Idle);

        fsm = new FSMSystem();
        fsm.AddState(idle);
        fsm.AddState(patrol);
        fsm.AddState(chase);
        fsm.AddState(attack);
    }

	void FixedUpdate ()
    {
        fsm.CurrentState.Reason(player, gameObject);
        fsm.CurrentState.Act(player, gameObject);
	}
}
