﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Enemy))]
[System.Serializable]
public class LootDropper : MonoBehaviour
{
    public LootTable lootTable;
    public int numOfItemsToDrop = 1;

    public Transform dropPosition;

    void OnValidate()
    {
        lootTable.ValidateTable();
    }

    public void DropItem()
    {
        Debug.Log("Dropped an item");
        for (int i = 0; i < numOfItemsToDrop; i++)
        {
            LootItem item = lootTable.PickItemToDrop();
            GameObject drop = Instantiate(item.lootItem);
            if (dropPosition == null)
                drop.transform.position = transform.position;
            else
                drop.transform.position = dropPosition.transform.position;
        }
    }
}
