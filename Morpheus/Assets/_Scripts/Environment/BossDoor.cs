﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoor : MonoBehaviour {

    public bool isLock;

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isLock = true;
        }
    }



    // Use this for initialization
    void Start () {
        isLock = false;

    }
	
	// Update is called once per frame
	void Update () {
        if (isLock)
        {
            this.gameObject.layer = 9;
            this.gameObject.transform.GetChild(0).transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        }
	}
}
