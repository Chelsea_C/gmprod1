﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorSpecial02 : MonoBehaviour {

    public bool isOpen;
    public GameObject bridgePlatform;

    // Use this for initialization
    void Start () {
        isOpen = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (isOpen)
        {
            bridgePlatform.GetComponent<BossDoorSpecial02_01>().isActive = true;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isOpen = true;
        }
    }

}
