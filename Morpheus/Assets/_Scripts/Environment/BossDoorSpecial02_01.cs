﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorSpecial02_01 : MonoBehaviour {

    public bool isActive;
    public GameObject triggerTop;
    public GameObject triggerBot;

    // Use this for initialization
    void Start () {
        isActive = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (isActive)
        {
            if (triggerBot.GetComponent<BossDoorSpecial02_Bot>().goTop)
            {
                this.transform.Translate(Vector3.up * Time.deltaTime, Space.World);
            }

            if (triggerTop.GetComponent<BossDoorSpecial02_Top>().goBot)
            {
                this.transform.Translate(Vector3.down * Time.deltaTime, Space.World);
            }
        }
    }
}
