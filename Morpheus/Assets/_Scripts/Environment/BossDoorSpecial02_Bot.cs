﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorSpecial02_Bot : MonoBehaviour {

    public bool goTop;
    public GameObject triggerTop;

    public void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("GoTop");
        if (collision.gameObject.tag == "Elevator")
        {
            goTop = true;
            triggerTop.GetComponent<BossDoorSpecial02_Top>().goBot = false;
        }
    }

    // Use this for initialization
    void Start () {
        goTop = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
