﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(PlayerController), typeof(PlayerInput))]
public class Player : MonoBehaviour
{
    public PlayerData data;
    public Image healthBar;
    public Image face;
    
    int currentHealth;
    int bonusAttackFromItems;
    Enemy closestEnemy;

    void Start()
    {
        // Initialize stats
        data.currentHealth = data.InitMaxHealth;
        data.currentMaxHealth = data.InitMaxHealth;
        data.currentAttack = data.InitAttack;
        data.currentMaxSlots = data.InitMaxSlots;

        Debug.Log(data.currentMaxSlots);
        healthBar.fillAmount = 1;

        SetMaxSlots();

    }

    void Update()
    {
        // Update health bar
        healthBar.fillAmount = (float) data.currentHealth / data.currentMaxHealth;

        //TESTING
        if(Input.GetKeyDown(KeyCode.R))
        {
            TakeDamage(10);
        }

    }

    public void TakeDamage(int damage)
    {
        if (data.currentHealth > 0)
            data.currentHealth -= damage;
        else
            Die();
    }

    void Die()
    {
        //TODO
        gameObject.SetActive(true);
        Debug.Log("Player is dead");
    }

    public void SetMaxSlots()
    {
        //Inventory.instance.m
    }

    public void UpdateAttackStats()
    {
        GetItemAttackBonus();
        data.currentAttack += bonusAttackFromItems;
        Debug.Log("Attack stat updated: " + data.currentAttack);
    }

    public void ApplyDamage(Collider2D col, LayerMask enemyLayer)
    {
        Collider2D[] cols = Physics2D.OverlapBoxAll(col.bounds.center, col.bounds.extents, col.transform.rotation.x, enemyLayer);

        if (cols == null)
        {
            Debug.Log("No enemies found");

            closestEnemy = null;
            return;
        }

        ClosestEnemy(cols);
        if(closestEnemy != null)
            closestEnemy.TakeDamage(data.currentAttack);

        //// if not single target
        //foreach (Collider2D c in cols)
        //{
        //    c.GetComponent<Enemy>().TakeDamage(data.currentAttack);
        //}
    }

    void ClosestEnemy(Collider2D[] enemies)
    {
        float smallestDistance = Mathf.Infinity;
        float distanceToPlayer;
        for(int i = 0; i < enemies.Length; i++)
        {
            distanceToPlayer = Vector2.Distance(transform.position, enemies[i].transform.position);
            if(distanceToPlayer < smallestDistance)
            {
                smallestDistance = distanceToPlayer;
                closestEnemy = enemies[i].GetComponent<Enemy>();
            }
        }
    }

    void GetItemAttackBonus()
    {
        bonusAttackFromItems = 0;
        foreach (ComponentSO c in Inventory.instance.equipped)
        {
            int count = Inventory.instance.GetCount(c);
            if (count > 1)
                bonusAttackFromItems += c.power;
        }
    }

    //private void OnTriggerEnter2D(Collider2D other)
    //{
    //    if(other.tag == "Enemy")
    //    {
    //        TakeDamage(other.GetComponent<EnemyController>().attack);
    //    }
    //}

}
