﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerInput : MonoBehaviour
{
    public bool gameIsPaused;

    float timeUntilNextAttack = 0.0f;
    float timeUntilNextFire = 0.0f;
    Vector2 directionalInput;
    PlayerController controller;
    
    void Start()
    {
        controller = GetComponent<PlayerController>();
    }

    void Update()
    {
        if (gameIsPaused)
            return;

        // Movement
        directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        controller.SetDirectionalInput(directionalInput);

        // Jumping
        if (Input.GetKeyDown(controller.data.jump))
        {
            controller.OnJumpInputDown();
        }
        if (Input.GetKeyUp(controller.data.jump))
        {
            controller.OnJumpInputUp();
        }

        // Attacking
        if (Input.GetKeyDown(controller.data.attack))
        {
            if (timeUntilNextAttack <= 0 && controller.data.isMelee)
            {
                controller.OnMelee();
                timeUntilNextAttack = controller.data.meleeCooldown;
            }

            if (timeUntilNextFire <= 0 && controller.data.isRanged)
            {
                controller.OnRangedAttack();
                timeUntilNextFire = controller.data.fireCooldown;
            }
        }

        timeUntilNextFire -= Time.deltaTime;
        timeUntilNextAttack -= Time.deltaTime;

        // Clearing inventory slots
        if(Input.GetKeyDown(controller.data.purge))
        {
            Inventory.instance.Clear();
        }

        // Pausing
        if(Input.GetKeyDown(controller.data.pause))
        {
            //Debug.Log("pause");
        }
    }

}
