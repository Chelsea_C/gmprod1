﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour {

    public delegate void OnFocusChanged(Interactable item);
    public OnFocusChanged onFocusChangedCallback;

    public List<ComponentSO> interInRange;

    public float x, y, yOffset;

    public float interactionRadius = 1f;

    public LayerMask movementMask;
    public LayerMask interactionMask;

    [SerializeField]
    Interactable focus;
    Interactable closest;

    void Update()
    {
        SetFocus(GetNearestInteractable());

        if (focus != null)
            focus.Interact();
    }


    Interactable GetNearestInteractable()
    {
        Collider2D[] interactables = Physics2D.OverlapCircleAll(transform.position, interactionRadius, interactionMask);
        float distance = Mathf.Infinity;
        foreach (Collider2D interactable in interactables)
        {
            float interactableDistance = Vector2.Distance(interactable.transform.position, transform.position);
            if (interactableDistance < distance)
            {
                closest = interactable.GetComponent<Interactable>();
                distance = interactableDistance;
            }
        }
        return closest;
    }

    void SetFocus(Interactable newFocus)
    {
        if (onFocusChangedCallback != null)
            onFocusChangedCallback.Invoke(newFocus);

        if(focus != newFocus && focus != null)
        {
            focus.OnDefocused();
        }

        focus = newFocus;

        if(focus!= null)
        {
            focus.OnFocused(transform);
        }
    }

    #region Gizmos
    private void OnDrawGizmosSelected()
    {
        //Vector3 pos = new Vector3( transform.position.x, transform.position.y + yOffset, transform.position.x);
        Gizmos.color = Color.yellow;
        //Gizmos.DrawWireCube(pos, new Vector3(x, y, 0));
        //Gizmos.DrawWireSphere(transform.position, interactionRadius);
    }
    #endregion

}
