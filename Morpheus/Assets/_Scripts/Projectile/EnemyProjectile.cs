﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : Projectile {

    //public LayerMask targetLayer = LayerMask.NameToLayer("Player");

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            col.GetComponent<Player>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
