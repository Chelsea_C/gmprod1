﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : Projectile {

    //public LayerMask targetLayer = LayerMask.NameToLayer("Enemy");

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Non-target layer detected");
        //if (col.gameObject.layer != targetLayer)
        //    return;



        if (col.tag == "Enemy")
        {
            col.GetComponent<Enemy>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
