using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue[] dialogue;
    public DialogueManager dm;
    public int currentDialogueIndex = 0;
    public Canvas continueButton;

    void Start()
    {
        currentDialogueIndex = 0;
        continueButton.enabled = false;
        TriggerDialogue();
    }

	public void TriggerDialogue()
	{
		dm.StartDialogue(dialogue[currentDialogueIndex]);
	}

    public void LoadNextDialogue()
    {
        if (currentDialogueIndex == (dialogue.Length - 1))
        {
            continueButton.enabled = true;
            return;
        }
        currentDialogueIndex += 1;
        TriggerDialogue();
    }
}
