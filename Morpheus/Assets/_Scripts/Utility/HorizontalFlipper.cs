﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalFlipper : MonoBehaviour {

    [HideInInspector]
    public bool isFacingRight = true;

	void Update ()
    {
        if (isFacingRight)
            transform.rotation = Quaternion.Euler(0, 180f, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);
	}
}
