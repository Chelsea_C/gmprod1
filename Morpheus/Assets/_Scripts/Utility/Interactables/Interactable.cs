﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractableType
{
    pickup,
    door
}

[RequireComponent(typeof(Collider2D))]
public class Interactable : MonoBehaviour {

    public InteractableType type;
    public float radius = 0.5f;

    public Transform interactionTransform;

    bool isFocus = false;
    bool hasInteracted = false;

    Transform player;


    void Update()
    {
        if (isFocus)
        {
            float distanceToPlayer = Vector2.Distance(player.position, interactionTransform.position);
            if (!hasInteracted && distanceToPlayer <= radius)
            {
                hasInteracted = true;
                Interact();
            }
        }
    }

    public virtual void Interact()
    {
        
    }

    public void OnFocused(Transform playerTransform)
    {
        isFocus = true;
        hasInteracted = false;
        player = playerTransform;
    }

    public void OnDefocused()
    {
        isFocus = false;
        hasInteracted = false;
        player = null;
    }

    public void Highlight()
    {
        GetComponent<SpriteOutline>().enabled = true;
        GetComponent<SpriteOutline>().color = Color.yellow;
        GetComponent<SpriteOutline>().outlineSize = 2;
    }

    public void RemoveHighlight()
    {
        GetComponent<SpriteOutline>().enabled = false;
    }

    //#region Gizmos
    //private void OnDrawGizmosSelected()
    //{
    //    float colRadius = gameObject.GetComponent<CircleCollider2D>().radius;
    //    Gizmos.color = Color.yellow;
    //    Gizmos.DrawWireSphere(transform.position, radius);
    //}
    //#endregion
}
