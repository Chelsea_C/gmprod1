﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LootItemBase<T>
{
    public T lootItem;
    public float probabilityWeight;
    public float probabilityPercent;

    [HideInInspector]
    public float probabilityRangeFrom;
    [HideInInspector]
    public float probabilityRangeTo;
}
