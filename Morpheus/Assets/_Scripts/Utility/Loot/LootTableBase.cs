﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LootTableBase<T, U> where T : LootItemBase<U>
{
    [SerializeField]
    public List<T> lootItems;

    float totalWeight;

    public void ValidateTable()
    {
        if (lootItems != null && lootItems.Count > 0)
        {

            float currentTotalWeight = 0f;

            foreach (T lootItem in lootItems)
            {

                if (lootItem.probabilityWeight < 0f)
                {
                    lootItem.probabilityWeight = 0f;
                }
                else
                {
                    lootItem.probabilityRangeFrom = currentTotalWeight;
                    currentTotalWeight += lootItem.probabilityWeight;
                    lootItem.probabilityRangeTo = currentTotalWeight;
                }

            }

            totalWeight = currentTotalWeight;

            // Calculate percentage of item drop select rate.
            foreach (T lootItem in lootItems)
            {
                lootItem.probabilityPercent = ((lootItem.probabilityWeight) / totalWeight) * 100;
            }

        }
    }

    public T PickItemToDrop()
    {
        //float cumulativeValue = 0;
        //float randomValue = Random.Range(0, totalWeight);
        //return null;

        float pickedNumber = Random.Range(0, totalWeight);
        
        foreach (T lootItem in lootItems)
        {
            if (pickedNumber > lootItem.probabilityRangeFrom && pickedNumber < lootItem.probabilityRangeTo)
            {
                return lootItem;
            }
        }
        
        Debug.LogError("Item could not be picked; Loot table may be empty");
        return lootItems[0];
    }
}
