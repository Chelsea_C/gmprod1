﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBindingsUI : MonoBehaviour {

    public PlayerControllerData data;
    public Text left;
    public Text right;
    public Text jump;
    public Text attack;
    public Text purge;
    public Text pause;

    void Start()
    {
        left.text = data.left.ToString();
        right.text = data.right.ToString();
        jump.text = data.jump.ToString();
        attack.text = data.attack.ToString();
        purge.text = data.purge.ToString();
        pause.text = data.pause.ToString();
    }

}
