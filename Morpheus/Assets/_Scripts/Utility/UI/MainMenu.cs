﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Scene[] levels;

    public void StartGame()
    {

    }

    public void GoToLevel(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void Options()
    {

    }

    public void Quit()
    {
        Debug.Log("Exiting game");
        Application.Quit();
    }
    

}
