﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public Canvas pauseMenu;
    public GameObject player;

    bool isPaused = false;

    void Awake()
    {
        pauseMenu.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isPaused)
            {
                Unpause();
            }
            
            if(!isPaused)
            {
                Pause();
            }
        }

        if (isPaused)
        {
            Time.timeScale = 0.0f;
        }
        else
            Time.timeScale = 1.0f;
    }

    public void OnQuit()
    {
        Application.Quit();
    }


    public void Unpause()
    {
        pauseMenu.enabled = false;
        isPaused = false;
        player.GetComponent<PlayerInput>().gameIsPaused = false;

    }

    public void Pause()
    {
        pauseMenu.enabled = true;
        isPaused = true;
        player.GetComponent<PlayerInput>().gameIsPaused = true;
    }
}
