﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {

    public void OnLoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void OnLoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

}
